#!/user/bin/env groovy
package com.example

class Docker implements Serializable { // Serializable: Save the state of the execution if pipeline is paused and  resumed

    def script

    Docker(script) { // Pipeline syntax/cmds/methods not available in Src. Script is the solution to this problem.
        this.script = script
    }

    def buildDockerImage(String imageName) {
        script.echo 'Building the docker image for ${script.BRANCH_NAME} branch}'
        script.sh "docker build -t $imageName ."
    }

    def dockerLogin() {
        script.withCredentials([script.usernamePassword(credentialsId: 'DockerHubLogin', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
            script.sh "echo '${script.PASS}' | docker login -u '${script.USER}' --password-stdin"
        }
    }

    def dockerPush(String imageName) {
        script.sh "docker push $imageName"
    }

}
